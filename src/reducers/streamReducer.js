import _ from "lodash";

export default (state = [], action) => {
    switch(action.type){
        case 'DELETE_STREAM':
            return _.omit(state, [action.payload] );
        case 'LIST_STREAMS':
            return {
                ...state,
                ..._.mapKeys(action.payload, 'id')
            };
        case 'GET_STREAM':
        case 'EDIT_STREAM':
        case 'CREATE_STREAM':
            return {
                ...state,
                [action.payload.id]: action.payload
            };
        default:
            return state;
    }
}