import ajax from '../api';

let _auth = null
let _useCallback = (callback, args) => {
    if(callback != undefined){
        callback(args);
    }
};

export const loadAuth = (callback) => {
    return (dispatch, getState) => {
        try{

            window.gapi.load('client:auth2', () => {
                window.gapi.auth2.init({
                    client_id: 'YOURCLIENTIDHERE',
                    scope: 'email'
                }).then(() => {
                    _auth = window.gapi.auth2.getAuthInstance();
                    dispatch({
                        type: 'AUTH_LOAD',
                        payload: ( _auth.isSignedIn.get() )?_auth.currentUser.get().getId():null
                    });
                });
            });

        }
        catch(error){

            _useCallback(callback,{error:error});

        }
    };
};

export const signIn = (callback) => {
    return (dispatch, getState) => {
        try{

            _auth.signIn()
                .then(
                    (result) => {
                        dispatch({
                            type: 'SIGN_IN',
                            payload: _auth.currentUser.get().getId()
                        });
                    },
                    (error) => {
                        _useCallback(callback,{error:error});
                    }
                );
    
        }catch(error){
            _useCallback(callback,{error:error});
        };
    };
};

export const signOut = (callback) => {
    return (dispatch, getState) => {
        try{
            if(_auth.isSignedIn.get()){
                _auth.signOut().then(
                    (result) => {
                        dispatch({
                            type: 'SIGN_OUT'
                        });
                    },
                    (error) => {
                        _useCallback(callback,{error:error});
                    }
                );
            }else{
                _useCallback(callback,{error:'NOT_SIGNED_IN'});
            }
        }
        catch(error){
            _useCallback(callback,{error:error});
        };
    }
}

export const createStream = ({form}, callback) => {
    return (dispatch, getState) => {
        ajax.post('/streams', form).then(
            (response)=>{
                dispatch({
                    type: 'CREATE_STREAM',
                    payload: response.data
                });
                _useCallback(callback,response);
            },
            (error)=>{
                _useCallback(callback,{ error: error });
            }
        );
    };
};

export const listStreams = (callback) => {
    return (dispatch, getState) => {
        ajax.get('/streams').then(
            (response)=>{
                dispatch({
                    type: 'LIST_STREAMS',
                    payload: response.data
                });
                _useCallback(callback,response);
            },
            (error)=>{
                _useCallback(callback,{ error: error });
            }
        );
    };
};

export const getStream = ({id},callback) => {
    return (dispatch, getState) => {
        ajax.get(`/streams/${id}`).then(
            (response)=>{
                dispatch({
                    type: 'GET_STREAM',
                    payload: response.data
                });
                _useCallback(callback,response);
            },
            (error)=>{
                _useCallback(callback,{ error: error });
            }
        );
    };
};

export const editStream = ({id, form}, callback) => {
    return (dispatch, getState) => {
        ajax.patch(`/streams/${id}`, form).then(
            (response)=>{
                dispatch({
                    type: 'EDIT_STREAM',
                    payload: response.data
                });
                _useCallback(callback,response);
            },
            (error)=>{
                _useCallback(callback,{ error: error });
            }
        );
    };
};

export const deleteStream = ({id}, callback) => {
    return (dispatch, getState) => {
        ajax.delete(`/streams/${id}`).then(
            (response)=>{
                dispatch({
                    type: 'DELETE_STREAM',
                    payload: id
                });
                _useCallback(callback,response);
            },
            (error)=>{
                _useCallback(callback,{ error: error });
            }
        );
    };
};
