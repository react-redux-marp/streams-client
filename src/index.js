import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app.js';
import { Provider } from 'react-redux';
import reducers from './reducers';
import { createStore, applyMiddleware, compose } from 'redux'; 
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
ReactDOM.render(
    <Provider store={createStore(reducers, composeEnhancers( applyMiddleware(thunk) ) )}>
        <App/>
    </Provider>,
    document.querySelector('#root')
);