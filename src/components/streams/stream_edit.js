import React from 'react';
import _ from 'lodash';
import { StreamForm } from './stream_form';
import { connect } from 'react-redux';
import { getStream, editStream } from '../../actions';

class StreamEdit extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            sending: false,
            error: false
        };
    }

    componentDidMount(){
        if(this.props.stream == undefined){
            this.props.getStream({
                    id: this.props.match.params.id
                },
                (response) => {
                    if(response.error){ this.setState({error: String(response.error) });return; }
                });
        }
    }

    onSubmit = (values) => {
        if(!this.state.sending){
            this.setState({ sending: true});
            return new Promise(( resolve, reject ) => {
                this.props.editStream({
                    id: this.props.match.params.id,
                    form: _.pick(values, ['title', 'description'])
                }, (response) => {
                    if(response.error){ reject(String(response.error));return; }
                    if(response.status == 200){
                        resolve(true);
                        this.setState({ sending: false});
                        this.props.history.push('/');
                    }
                });
            });
        }
    };

    renderError(){
        if(this.state.error){
            return (<div>
                {this.state.error}
            </div>);
        }else return;
    }

    renderForm(){
        if(this.props.stream){
            return (
                <StreamForm
                    disabled={this.state.sending}
                    initialValues={_.pick(this.props.stream, ['title', 'description'])}
                    onSubmit={this.onSubmit}>
                    <h3>Edit Stream</h3>
                </StreamForm>
            );
        }else return;
    }

    render(){
        return (
            <div className="wrapper">
                {this.renderForm()}
                {this.renderError()}
            </div>
        );
    }

};

const mapStateToProps = (state, props) => {
    return {
        stream: state.streams[props.match.params.id]
    };
};
export default connect(
    mapStateToProps,
    {
        getStream: getStream,
        editStream: editStream
    }
)(StreamEdit);