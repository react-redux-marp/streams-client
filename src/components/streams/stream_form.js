import React from 'react';
import _ from 'lodash';
import {Field, reduxForm} from 'redux-form';

class Form extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            sentConfirmation: false,
            submitError: false
        };
        this.timer = null
    }

    componentWillUnmount(){
        clearTimeout(this.timer);
    }
    sentConfirmation(){
        if(this.state.sentConfirmation){
            this.timer = setTimeout(()=>{
                this.setState({sentConfirmation: false});
            }, 1500);
            return <p className="position-relative float-left ml-2">Form submitted!</p>;
        }else return;
    }
    onSubmit = (values) => {
        const promise = this.props.onSubmit(values);
        if(promise instanceof Promise){
            promise
                .then((response) => {
                    this.setState({ sentConfirmation: true });
                })
                .catch((error) => {
                    this.setState({ submitError: String(error) });
                });
        }
    }
    submitError(){
        if(this.state.submitError){
            return <div className="mt-2 alert alert-warning" role="warning">
                {this.state.submitError}
            </div>
        }else return;
    }
    render(){
        const { handleSubmit, className, disabled } = this.props;
        return (
            <form
                className={`${className||''}`}
                disabled={disabled}
                onSubmit={handleSubmit(this.onSubmit)}>
                {this.props.children}
                <Title/>
                <Description/>
                <Submit/>
                {this.sentConfirmation()}
                {this.submitError()}
            </form>
        );
    }
}

//connecting to redux form
export const validate = (values) => {
    let errors = {};

    if(!values.title || values.title == ''){
        errors['title'] = 'Please enter a valid title';
    }

    if(!values.description || values.description == ''){
        errors['description'] = 'Please enter a valid description';
    }

    return errors;
}

const _inputField = (props) => {

    let showErrors;
    if(props.meta.touched && props.meta.error){
        showErrors = (
            <div className="mt-2 alert alert-warning" role="warning">
                {props.meta.error}
            </div>
        );    
    }

    return (
        <div className={`form-group field ${props.type=='hidden'?'d-none':''}`}>
            <label htmlFor={`${props.input.name}`}>{props.label}</label>
            <input {...props.input} type={props.type||'text'} placeholder={props.placeholder} className={`form-control ${props.className}`}></input>
            {showErrors}
        </div>
    );

}

const _newField = (props) => {
    return (
        <Field component={_inputField} {...props} />
    );
}

export const Description = (props) => {
    return (
        <_newField name="description" component={_inputField} label="Description" {...props} />
    );
}

export const Title = (props) => {
    return (
        <_newField name="title" component={_inputField} label="Title" {...props} />
    );
}

export const UserID = (props) => {
    return (
        <_newField name="userId" component={_inputField} label="UserID" type="hidden" {...props} />
    );
}

export const ID = (props) => {
    return (
        <_newField name="id" component={_inputField} label="id" type="hidden" {...props} />
    );
}

export const Submit = (props) => {
    return (
        <button
            {...props}
            type="submit"
            className={`btn btn-success position-relative float-left ${props.className}`}
            disabled={props.disabled?'disabled':false}>
            Submit
        </button>
    );
}

export const getNewForm = (configObj) => {
    return (form) => {
        return reduxForm({
            form: configObj.form,
            validate: validate
        })(form);
    };
}

export const getStreamForm = (configObj) => {
    return getNewForm({
        form: configObj.form
    })(Form);
}

export const StreamForm = getStreamForm({ form: 'stream_form' })