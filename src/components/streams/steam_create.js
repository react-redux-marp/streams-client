import React from 'react';
import { connect } from 'react-redux';
import { createStream } from  '../../actions';
import { StreamForm } from './stream_form';
import _ from 'lodash';

class StreamCreate extends React.Component{
    state = {
        sending: false
    };

    onSubmit = (values) => {
        if(!this.state.sending){
            this.setState({ sending: true});
            return new Promise((resolve, reject) => {
                this.props.createStream({
                    form:_.omit({
                        ...values,
                        userId: this.props.auth.userId
                    }, ['id'])
                },
                (response) => {
                    if(response.error){ reject(response.error);return false; }
                    if(response.status == 201){
                        this.setState({ sending: false });
                        resolve(true);
                        this.props.history.push('/');
                    }
                });    
            });
        }
    }

    render(){

        if(!this.props.auth.isSignedIn){ return <div>Please sign in...</div>; }
        return (
            <div className="wrapper">
                <StreamForm
                    onSubmit={this.onSubmit}
                    disabled={this.state.sending}>
                    <h3>Create Stream</h3>
                </StreamForm>
            </div>
        );    
    }

}

//connecting to redux
const mapStateToProps = (state) => {
    return {
        auth: state.auth
    };
};

const StreamCreateSynced = connect(
    mapStateToProps,
    {
        createStream: createStream
    }
)(StreamCreate);

export default StreamCreateSynced;