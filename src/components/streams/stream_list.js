import React from 'react';
import _ from 'lodash';
import { listStreams } from '../../actions';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux' 

const Stream = (props) => {
    let streamActions;
    if(props.isOwner){
        streamActions = (
            <div className="stream-actions position-relative float-right">
                <Link to={`/streams/delete/${props.id}`} className="btn btn-danger mr-2 position-relative float-left">
                    Delete
                </Link>
                <Link to={`/streams/edit/${props.id}`} className="btn btn-success position-relative float-left">
                    Edit
                </Link>
            </div>
        );
    }

    return (
        <div className="stream list-group-item d-flex align-items-center" id={`stream_${props.id}`}>
            <i className="fa fa-camera mr-3"></i>
            <div className="content flex-grow-1">
                <Link to={`/streams/show/${props.id}`} className="title m-0">{props.title}</Link>
                <p className="description m-0">{props.description}</p>
            </div>
            {streamActions}
        </div>
    );
};

class StreamList extends React.Component {

    renderStreams = (streams) => {
        return streams.map(
            el => <Stream key={el.id} {...el} isOwner={el.userId==this.props.auth.userId} />
        );
    }

    render(){
        if(!this.props.auth.isSignedIn){
            return <div>Please sign in!</div>;
        }else if(this.props.count == 0){ this.props.listStreams(); }
        return (
            <div>
                <div className="owned mb-4 d-block position-relative">
                    <ul className="list-group">
                        {this.renderStreams(this.props.ownStreams)}
                    </ul>
                    <Link to="/streams/new" className="btn btn-warning position-relative float-right mt-2">
                        Create stream
                    </Link>
                    <div className="w-100 clearfix"/>
                </div>
                <ul className="others list-group">
                    {this.renderStreams(this.props.otherStreams)}
                </ul>
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    const streams = Object.values(state.streams);
    return {
        count: streams.length,
        ownStreams: streams.filter( el => el.userId == state.auth.userId),
        otherStreams: streams.filter( el => el.userId != state.auth.userId),
        auth: state.auth
    };
};

export default connect(mapStateToProps, { listStreams: listStreams })(StreamList);