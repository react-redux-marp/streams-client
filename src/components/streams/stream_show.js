import React from 'react';
import {connect} from 'react-redux';
import {getStream} from '../../actions';
import flv from 'flv.js';

class StreamShow extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            sending: false,
            error: false
        };
        this.videoRef = React.createRef();
    }
    componentDidMount(){
        const { id } = this.props.match.params;
        if(this.props.stream == undefined){
            this.props.getStream({ id: id },
                (response) => {
                    if(response.error){ this.setState({error: String(response.error)});return; }
                });
        }
        this.buildPlayer();
    }
    componentDidUpdate(){
        this.buildPlayer();
    }
    componentWillUnmount(){
        this.player.destroy();
    }
    buildPlayer = () => {
        if(this.player || this.props.stream == undefined){
            return;
        }

        const { id } = this.props.match.params;
        this.player = flv.createPlayer({
            type: 'flv',
            url: `http://localhost:8000/live/${id}.flv`
        });
        this.player.attachMediaElement(this.videoRef.current);
        this.player.load();

    }
    renderError = () => {
        if(this.state.error){
            return <p>{this.state.error}</p>;
        }
    }
    renderStream = () => {
        if(this.props.stream){
            return (
                <div className="stream" data-id={`${this.props.stream.id}`}>
                    <div className="title">{this.props.stream.title}</div>
                    <div className="description">{this.props.stream.description}</div>
                </div>
            );
        }
    }
    render(){
        return (
            <React.Fragment>
                <video ref={this.videoRef} style={{ width:'100%' }} controls/>
                {this.renderStream()}
                {this.renderError()}
            </React.Fragment>
        );
    }
};

const mapStateToProps = (state, props) => {
    return {
        stream: state.streams[props.match.params.id]
    };
}

export default connect(mapStateToProps, {
    getStream
})(StreamShow);