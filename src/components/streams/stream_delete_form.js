import React from 'react';
import { connect } from 'react-redux';
import { ConfirmationForm } from '../confirmation_form';
import { getStream, deleteStream } from '../../actions';

class StreamDeleteForm extends React.Component {
    state = {
        sending: false,
        error: false
    }

    componentDidMount(){
        if(this.props.stream == undefined){
            this.props.getStream({
                id: this.props.match.params.id
            },
            (response) => {
                if(response.error){ this.setState({error: String(response.error) });return; }
            });
        }
    }
    onSubmit = () => {
        if(!this.state.sending){
            this.setState({ sending: true });
            return new Promise((resolve, reject) => {
                this.props.deleteStream({
                    id: this.props.stream.id
                },
                (response) => {
                    if(response.error){ reject(String(response.error));return; }
                    this.setState({ sending: false });
                    resolve(true);
                    this.props.history.push('/');
                });
            });
        }
    }
    renderError(){
        if(this.state.error){
            return (<div>
                {this.state.error}
            </div>);
        }else return;
    }
    renderForm(){
        if(this.props.stream){
            return (
                <ConfirmationForm
                    confirmationMessage="Stream deleted!"
                    submitLabel="Delete"
                    disabled={this.state.sending}
                    onSubmit={this.onSubmit}>
                    <p>Are you sure you want to delete <strong>"{this.props.stream.title}"</strong> stream?</p>
                </ConfirmationForm>
            );
        }
    }
    render(){
        return (
            <div className="stream-delete-wrapper">
                {this.renderForm()}
                {this.renderError()}
            </div>
        );
    }
};

const mapStateToProps = (state, props) => {
    return {
        stream: state.streams[props.match.params.id]
    };
};

export default connect(mapStateToProps, {
    getStream, deleteStream
})(StreamDeleteForm);