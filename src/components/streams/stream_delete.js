import _ from 'lodash';
import React from 'react';
import Modal from '../modal.js';
import Form from './stream_delete_form';

export default (props) => {
    return (
        <Modal title="Delete Stream?" >
            <Form {..._.pick(props, ['history', 'match'])}/>
        </Modal>
    );
}