import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';

const modalRoot = document.querySelector('#modal');

class Modal extends React.Component {

    render(){
        return ReactDOM.createPortal(
            <div>
                <div className={`modal fade show d-block ${this.props.className}`} tabindex="-1" role="dialog">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">{this.props.title}</h5>
                                <Link to={`${this.props.closesTo||'/'}`} type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span>&times;</span>
                                </Link>
                            </div>
                            <div className="modal-body">
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop fade show d-block"></div>
            </div>
            , modalRoot
        );
    }

};

export default Modal;