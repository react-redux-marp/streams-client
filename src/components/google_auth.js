import React from 'react';
import { connect } from 'react-redux';
import { signIn, signOut} from '../actions'

const AuthButton = (props) => {
    const onAuthClick = () => {
        props.onCLick();
    };
    let AuthLabel;
    switch(props.isSignedIn){
        case true:
            AuthLabel = "Sign Out?";
            break;
        case false:
            AuthLabel = "Sign in!";
            break;
        default:
            AuthLabel = "Sign in!"; //OAth2 not loaded
    }
    return (
        <a className={`authbutton ${props.className}`} onClick={onAuthClick}>{props.children} {AuthLabel}</a>
    );
};

class GoogleAuth extends React.Component{
    signIn = () => {
        console.log(this.props);
        if(this.props.isSignedIn == true){
            this.props.signOut();
        }else if(this.props.isSignedIn != true){
            this.props.signIn();
        }
    }
    render(){
        return (
            <div className={`google-auth ${this.props.className}`}>
                <AuthButton className="btn btn-primary btn-lg" isSignedIn={this.props.isSignedIn} onCLick={this.signIn}>
                   <i className="fa fa-google"></i>
                </AuthButton>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isSignedIn: state.auth.isSignedIn
    }
}

export default connect(mapStateToProps, {
    signIn: signIn, signOut: signOut
})(GoogleAuth);