import React from 'react';
import _ from 'lodash';
import {reduxForm} from 'redux-form';
import {Link} from 'react-router-dom'

class Form extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            sentConfirmation: false,
            submitError: false
        };
        this.timer = null
    }
    sentConfirmation(){
        if(this.state.sentConfirmation){
            this.timer = setTimeout(()=>{
                this.setState({sentConfirmation: false});
            }, 1500);
            return (
                <p className="position-relative float-left ml-2">
                    {this.props.confirmationMessage}
                </p>
            );
        }else return;
    }
    onSubmit = (values) => {
        const promise = this.props.onSubmit(values);
        if(promise instanceof Promise){
            promise
                .then((response) => {
                    this.setState({ sentConfirmation: true });
                })
                .catch((error) => {
                    this.setState({ submitError: String(error) });
                });
        }
    }
    submitError(){
        if(this.state.submitError){
            return <div className="mt-2 alert alert-warning" role="warning">
                {this.state.submitError}
            </div>
        }else return;
    }
    render(){
        const { handleSubmit, className, disabled } = this.props;
        return (
            <form
                className={`${className||''}`}
                disabled={disabled}
                onSubmit={handleSubmit(this.onSubmit)}>
                {this.props.children}
                <div>
                    <Link to="/" className="btn btn-success position-relative float-left mr-3">
                        Cancel
                    </Link>
                    <Submit>{this.props.submitLabel||'Submit'}</Submit>
                </div>
                {this.sentConfirmation()}
                {this.submitError()}
            </form>
        );
    }
}

export const Submit = (props) => {
    return (
        <button
            {...props}
            type="submit"
            className={`btn btn-danger position-relative float-left ${props.className}`}
            disabled={props.disabled?'disabled':false}>
            {props.children}
        </button>
    );
}

export const getConfirmationForm = (configObj) => {
    return reduxForm({
        form: configObj.form
    })(Form);
}

export const ConfirmationForm = getConfirmationForm({ form: 'confirmation_form' })