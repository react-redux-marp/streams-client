import React from 'react';
import {connect} from 'react-redux';
import {loadAuth} from '../actions';
import {BrowserRouter, Route, Link} from 'react-router-dom';
import StreamList from './streams/stream_list.js';
import StreamCreate from './streams/steam_create.js';
import StreamEdit from './streams/stream_edit.js';
import StreamDelete from './streams/stream_delete.js';
import StreamShow from './streams/stream_show.js';
import GoogleAuth from './google_auth.js';
import history from '../history.js';

class App extends React.Component{
    componentDidMount(){
        this.props.loadAuth();
    }
    render(){
        return (
            <div className="container my-3">
                <BrowserRouter history={history}>
                    <div className="row mb-3">
                        <div className="col-12 col-sm-6">
                            <h1>
                                <Link to="/" className="text-body">
                                    Streamy
                                </Link>
                            </h1>
                        </div>
                        <div className="col-12 col-sm-6 d-flex flex-row-reverse align-items-center">
                            <GoogleAuth/>
                            <Link to="/" className="mr-3 text-body">
                                All Streams
                            </Link>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-sm-12">
                            <Route path="/" exact component={StreamList}/>
                            <Route path="/streams/new" exact component={StreamCreate}/>
                            <Route path="/streams/edit/:id" exact component={StreamEdit}/>
                            <Route path="/streams/delete/:id" exact component={StreamDelete}/>
                            <Route path="/streams/show/:id" exact component={StreamShow}/>
                        </div>
                    </div>
                </BrowserRouter>    
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: { ...state.auth }
    };
};

export default connect(mapStateToProps,{ loadAuth: loadAuth })(App);